﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
 
public class AmmoManager : MonoBehaviour
{
    
    [SerializeField] Text scoreFB;
 
    private int scoreInt;

    
    // Start is called before the first frame update
    void Start()
    {
         scoreInt = 0;
        scoreFB.text = scoreInt.ToString("00");
    }
 
    public void AddScore(int value){
        scoreInt+=value;
        scoreFB.text = scoreInt.ToString("00");
    }
   
   
    
    
}