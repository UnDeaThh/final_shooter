﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerBehaviour : MonoBehaviour
{

    [SerializeField] AudioSource hurt;
    [SerializeField] AudioSource die;

    public float speed;
    private Vector2 axis;

    public Vector2 limits;
    
    Animator anim;
    public float maxHealth = 100f;
    private float currentHealth;
    public Image healthBar;
    public float maxShield = 100f;
    private float currentShield;
    public Image shieldBar;

    private bool shield = false;
    private bool shieldOff = true;
    [SerializeField] GameObject shieldoff;

    public Transform bulletSpawner;
    public GameObject normalBullet;
    public GameObject fbBullet;
    public GameObject tbBullet;


    [SerializeField] Text scoreFB;
    private int scoreIntFB;

    [SerializeField] Text scoreTB;
    private int scoreIntTB;


    //__________AMMO_________//

    
    




    void Awake(){
       
    }
    void Start()
    {
        anim = GetComponent<Animator>();
        currentHealth = maxHealth;
        currentShield = maxShield;

        healthBar.fillAmount = currentHealth / maxHealth;
        shieldBar.fillAmount = currentShield / maxShield;

        scoreIntFB = 0;
        scoreFB.text = scoreIntFB.ToString("00");
        
        scoreIntTB = 0;
        scoreTB.text = scoreIntTB.ToString("00");

      
        
    
    }

    public void AddScore(int value){
        scoreIntFB+=value;
        scoreFB.text = scoreIntFB.ToString("00");
    
    }

    public void AddScoreTB(int value){
        scoreIntTB+=value;
        scoreTB.text = scoreIntTB.ToString("00");
    
    }

    
    void Update()
    {  
        anim.SetBool("fly",true);     

        Shoot();
       
        TB();
        
        FB();
    
        
        if (Input.GetButton("Vertical2")){
            
            transform.Translate(0,speed*Time.deltaTime,0);
            
            
            
        }else if (Input.GetButton("Vertical")){
            transform.Translate(0,-speed*Time.deltaTime,0);
            
            
        }
        
        
        if (Input.GetButton("Horizontal2")){
            transform.Translate(speed*Time.deltaTime, 0 ,0);
            
            
        }else if (Input.GetButton("Horizontal")){
            transform.Translate(-speed*Time.deltaTime, 0 ,0);

        }
        if(currentHealth == 0.0f)
        {
            anim.SetBool("hurt", false);
            anim.SetBool("dead", true);
            speed = 0;
            StartCoroutine(Die());
        }
    }
    

    public void Shoot(){
       if(Input.GetButtonDown("Fire1")){
           Instantiate(normalBullet, bulletSpawner.position, bulletSpawner.rotation, null);
           
       }
    }
    public void FB(){
       
       if(Input.GetButtonDown("Fire2")){
          
           StartCoroutine(Fireball());
           
       }
       
       
    }
    public void TB(){
       
       if(Input.GetButtonDown("Fire3")){
           
           Instantiate(tbBullet, bulletSpawner.position, Quaternion.Euler(0,0,90), null);
           AddScoreTB(-1);
           
       }
       
       
    }
 
     
    
    public void OnTriggerEnter2D(Collider2D other) {
        if(other.tag == "Finish" || other.tag == "Enemy" || other.tag == "Arrow"){
            
            
            
            currentHealth -= 20f; 
            healthBar.fillAmount = currentHealth / maxHealth;
            StartCoroutine(Hurt());
             
            if(shield = true){
                currentHealth -= 0f;
                
                currentShield -= 33.4f; 
                shieldBar.fillAmount = currentShield / maxShield;
                StartCoroutine(Hurt());
                if(shieldBar.fillAmount <= 0){
                    StartCoroutine(Shield());
                }
             }

             
            
            
    }
             
            
        
            
        else if(other.tag == "Potion" ){
        
            currentHealth = 100f;
            healthBar.fillAmount = 1;
            
        }
        else if(other.tag == "Shield"){
            currentShield = 100f;
            shieldBar.fillAmount = currentShield;
            shieldoff.SetActive(false);
            shield = true;
            
        }  
            
        
          
    }

    

    IEnumerator Hurt(){
        anim.SetBool("hurt", true);
        hurt.Play();
        yield return new WaitForSeconds(0.7f);
        anim.SetBool("hurt", false);
    }

    IEnumerator Shield(){
            
        shieldoff.SetActive(true);
        currentShield = 100f;
        shieldOff = false;
        yield return new WaitForSeconds(0.1f);
        
    }


    IEnumerator Fireball(){
        Instantiate(fbBullet, bulletSpawner.position, bulletSpawner.rotation, null);
        AddScore(-1);
       
           
        yield return new WaitForSeconds(0.0f);
        

    }

    IEnumerator Die(){
            
            die.Play();
            yield return new WaitForSeconds(0.5f);
            die.Stop();
            yield return new WaitForSeconds(1.7f);
            SceneManager.LoadScene("GameOver");
    }



    




}
