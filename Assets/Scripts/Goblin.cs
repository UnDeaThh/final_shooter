﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
public class Goblin : MonoBehaviour
{
    //limites y arriba 0.5 y abajo -1.5
 
    [SerializeField] BoxCollider2D collider;
    [SerializeField] AudioSource audioSource;
 
    private float timeCounter;
    private float timeToShoot;
 
    private float timeShooting;
    public float speedx;
    private Vector2 axis;
 
    private bool isShooting;

    private ScoreManager sm;

    
 
    [SerializeField] GameObject bullet;

    private bool aimdead=false;
 
    private void Awake() {
        axis.x = transform.position.x;
        axis.y = transform.position.y;
        sm = (GameObject.Find("HUDCanvas")).GetComponent<ScoreManager>();
        Inicitialization();
    }
 
    protected virtual void Inicitialization(){
        timeCounter = 0.0f;
        timeToShoot = 1.0f;
        timeShooting = 1.0f;
        isShooting = false;
    }
 
    protected virtual void EnemyBehaviour(){
        if(axis.y < 0.5f && axis.y > -1.5f ){
        if(aimdead){
            return;
        }

        timeCounter += Time.deltaTime;
 
        if(timeCounter>timeToShoot){
            if(!isShooting){
                isShooting = true;
                Instantiate(bullet,this.transform.position,Quaternion.Euler(0,0,180),null);
            }
            if(timeCounter>(timeToShoot+timeShooting)){
                timeCounter = 0.0f;
                isShooting = false;
            }
        }else{
            transform.Translate(-speedx*Time.deltaTime,0,0);
        }
 
    }else {
        Destroy(this.gameObject);
    }
    }
 
    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
        EnemyBehaviour();
    }
 
     public void OnTriggerEnter2D(Collider2D other){
        if(other.tag == "Bullet") {
                Destroy(this.gameObject);
                 sm.AddScore(75);
    }else if(other.tag == "Finish"){
             Destroy(this.gameObject);
    }
 
 

}
}
