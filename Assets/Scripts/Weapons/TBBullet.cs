﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TBBullet : MonoBehaviour
{
    public float speed;

   [SerializeField] AudioSource audioSource;
    // Update is called once per frame
    void Start(){
        
    }
    
    void Update () {
        
        StartCoroutine(Destroyer());
        
    }


IEnumerator Destroyer(){
        transform.Translate (0 ,0, 0);
        audioSource.Play();
        yield return new WaitForSeconds(0.3f);
        audioSource.Stop();
        yield return new WaitForSeconds(0.5f);

        Destroy(this.gameObject);
}
}

    