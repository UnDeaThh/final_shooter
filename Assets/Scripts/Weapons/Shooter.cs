﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : Weapon
{

    public GameObject Bullet;
    
    public float cadencia;
    public AudioSource audioSource;

    public override float GetCadencia()
    {
        return cadencia;
    }

    public override void Shoot()
    {
        Instantiate (Bullet, this.transform.position, Quaternion.identity, null);
        audioSource.Play();
        
    }
}