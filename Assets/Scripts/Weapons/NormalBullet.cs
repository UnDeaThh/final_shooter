﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NormalBullet : MonoBehaviour
{
    public float speed;

    [SerializeField] AudioSource audioSource;

    
    // Update is called once per frame
    void Update () {
        StartCoroutine(Bullet());
    }

    IEnumerator Bullet(){
        transform.Translate (speed * Time.deltaTime,0, 0);
        audioSource.Play();
        
        yield return new WaitForSeconds(0.1f);
        audioSource.Stop();
    }

    public void OnTriggerEnter2D(Collider2D other){
        if (other.tag == "Finish" || other.tag == "Enemy" || other.tag == "Arrow"){
            Destroy (gameObject);
        }
    }

   

}
