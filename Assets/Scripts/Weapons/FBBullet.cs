﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FBBullet : MonoBehaviour
{
    public float speed;

    [SerializeField] AudioSource audioSource;
   
    // Update is called once per frame
    void Start(){
        
    }
    
    void Update () {
        StartCoroutine(Bullet());
        
    }
    IEnumerator Bullet(){
        transform.Translate (speed * Time.deltaTime,0, 0);
        audioSource.Play();
        
        yield return new WaitForSeconds(0.3f);
        audioSource.Stop();
    }

    public void OnTriggerEnter2D(Collider2D other){
        if (other.tag == "Finish"){
            Destroy (gameObject);
        }
    }

}
