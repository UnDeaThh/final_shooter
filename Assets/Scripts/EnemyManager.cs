﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyManager : MonoBehaviour
{
   public GameObject enemyPrefab;
   public float timeLaunchEnemy;

   private float currentTime = 0;
    

    void Update()
    {
        currentTime += Time.deltaTime;
        if(currentTime>timeLaunchEnemy){
            currentTime = 0;
            Instantiate(enemyPrefab,new Vector3(this.transform.position.x,Random.Range(-2.0f,1.3f), 8.97f),Quaternion.identity,this.transform);
        }
        
    }
}