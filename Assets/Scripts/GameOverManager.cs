﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverManager : MonoBehaviour
{
    [SerializeField] Text text;
    [SerializeField] Text record;

    // Start is called before the first frame update
    void Start()
    {
        float tiempoUltimaPartida = PlayerPrefs.GetFloat("tiempoMuerte");

        SetTime(tiempoUltimaPartida,text);



        float tiempoRecord = PlayerPrefs.GetFloat("record",0f);

        if(tiempoUltimaPartida>tiempoRecord){
            tiempoRecord = tiempoUltimaPartida;
            PlayerPrefs.SetFloat("record",tiempoRecord);
        }

        SetTime(tiempoRecord,record);
    }


    void SetTime(float gameTime, Text timer){
        int minutos, segundos;

        minutos = (int)(gameTime / 60f);
        segundos = (int)(gameTime % 60f);

        timer.text = minutos.ToString("000000"); 
    }

}
