﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FBAtk : MonoBehaviour
{
    public float speedx;
    private Vector2 axis;
    
    

    private AmmoManager am;

    
    // Update is called once per frame
     void Awake(){
        am = (GameObject.Find("FBManager")).GetComponent<AmmoManager>();
        
    }
    
    
    void Update () {
        transform.Translate(-speedx * Time.deltaTime, 0, 0); 
    }

    public void OnTriggerEnter2D(Collider2D other) {
        if(other.tag == "Player"){
            
            StartCoroutine(Ammo());
        }
    }
    IEnumerator Ammo(){
        Destroy(this.gameObject);
            am.AddScore(5);
            yield return new WaitForSeconds(0.1f);
    }
}
