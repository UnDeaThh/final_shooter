﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : MonoBehaviour
{
    public void OnTriggerEnter2D(Collider2D other) {
        if(other.tag == "Player"){
            Destroy(this.gameObject);
        }
            
    // Update is called once per frame
    void Update()
    {
        
    }
}
}
